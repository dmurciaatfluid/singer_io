from .core import (
    SingerRecord,
)

__all__ = [
    "SingerRecord",
]
