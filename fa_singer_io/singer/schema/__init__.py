from .core import (
    SingerSchema,
)

__all__ = [
    "SingerSchema",
]
