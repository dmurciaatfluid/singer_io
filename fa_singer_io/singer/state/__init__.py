from .core import (
    SingerState,
)

__all__ = [
    "SingerState",
]
